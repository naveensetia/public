#Check if the access to gitlab.com is existing or not
status=`echo 'ssh -T -o StrictHostKeyChecking=no git@gitlab.com|| true' | bash`
echo "Checking SSH access to gitLab."
if [[ $status == *"Welcome"* ]]
then
	echo "Congratulations! GitLab SSH access has already been set up."
else
    echo "Sorry! GitLab SSH access has not been set up yet."
fi    